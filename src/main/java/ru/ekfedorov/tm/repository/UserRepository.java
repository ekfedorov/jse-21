package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.model.User;

import java.util.Optional;
import java.util.stream.Collectors;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void clear() {
        list.stream()
                .filter(user -> user.getRole() == Role.USER)
                .collect(Collectors.toList())
                .forEach(list::remove);
    }


    @Override
    public Optional<User> findByLogin(final String login) {
        return list.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst();
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return findByLogin(login).isPresent();
    }



    @Override
    public Optional<User> removeByLogin(final String login) {
        return findByLogin(login).map(this::remove);
    }



}
