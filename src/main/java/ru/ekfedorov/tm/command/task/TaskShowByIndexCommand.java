package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Optional<Task> task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (!task.isPresent()) throw new NullTaskException();
        showTask(task.get());
        System.out.println();
    }

    @Override
    public String name() {
        return "task-view-by-index";
    }

}
