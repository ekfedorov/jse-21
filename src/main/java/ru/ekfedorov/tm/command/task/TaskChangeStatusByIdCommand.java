package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change task status by id.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Optional<Task> task = taskService.findOneById(userId, id);
        if (!task.isPresent()) throw new NullTaskException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Optional<Task> taskUpdated = taskService.changeStatusById(userId, id, status);
        if (!taskUpdated.isPresent()) throw new NullTaskException();
    }

    @Override
    public String name() {
        return "change-task-status-by-id";
    }

}
