package ru.ekfedorov.tm.command.system;

import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        final Collection<String> commands = serviceLocator.getCommandService().getListCommandName();
        System.out.println("[COMMANDS]");
        for (final String command : commands) {
            if (command != null) System.out.println(command);
        }
    }

    @Override
    public String name() {
        return "commands";
    }

}
