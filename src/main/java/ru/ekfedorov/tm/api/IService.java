package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.AbstractEntity;


public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
