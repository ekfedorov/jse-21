package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E>  {

    void clear(String userId);

    List<E> findAll(String userId, Comparator<E> comparator) throws Exception;

    Optional<E> findOneByIndex(String userId, Integer index) throws Exception;

    boolean removeOneByIndex(
            String userId, Integer index
    ) throws Exception;

    boolean removeOneByName(
            String userId, String name
    ) throws Exception;

    Optional<E> findOneByName(String userId, String name) throws Exception;

    List<E> findAll(String userId);

    Optional<E> findOneById(String userId, String id) throws Exception;

    boolean remove(String userId, E entity) throws Exception;

    boolean removeOneById(String userId, String id) throws Exception;

}
