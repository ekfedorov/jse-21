package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity) throws Exception;

    void clear();

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator) throws Exception;

    Optional<E> findOneById(String id) throws Exception;

    E remove(E entity) throws Exception;

    Optional<E> removeOneById(String id) throws Exception;

}
