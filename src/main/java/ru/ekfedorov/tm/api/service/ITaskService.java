package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.api.IBusinessService;
import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Task;

import java.util.Optional;

public interface ITaskService extends IBusinessService<Task> {

    Task add(final String userId, String name, String description) throws Exception;

}
