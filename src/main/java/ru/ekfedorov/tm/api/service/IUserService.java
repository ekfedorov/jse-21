package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.enumerated.Role;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IUserService {

    User add(User entity) throws Exception;

    void clear(Role role) throws Exception;

    User create(String login, String password) throws Exception;

    void create(String login, String password, String email) throws Exception;

    void create(String login, String password, Role role) throws Exception;

    List<User> findAll();

    List<User> findAll(Comparator<User> comparator) throws Exception;

    Optional<User> findByLogin(String login) throws Exception;

    Optional<User> findOneById(String id) throws Exception;

    boolean isLoginExist(String login) throws Exception;

    User remove(User entity) throws Exception;

    Optional<User> removeByLogin(String login) throws Exception;

    Optional<User> removeOneById(String id) throws Exception;

    void setPassword(String userId, String password) throws Exception;

    void userUpdate(
            String userId,
            String firstName,
            String lastName,
            String middleName
    ) throws Exception;

}
