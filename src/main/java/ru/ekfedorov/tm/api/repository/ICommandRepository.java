package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandArgs();

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    Collection<String> getCommandName();

    Collection<AbstractCommand> getCommands();

}
