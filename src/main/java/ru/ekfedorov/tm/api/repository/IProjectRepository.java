package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
