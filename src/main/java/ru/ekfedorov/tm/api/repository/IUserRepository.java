package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.User;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    Optional<User> findByLogin(String login);

    Boolean isLoginExist(String login);

    Optional<User> removeByLogin(String login);

}
