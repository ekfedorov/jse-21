package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;
import ru.ekfedorov.tm.model.Project;

import java.util.Optional;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IBusinessRepository<E>, IService<E> {

    Optional<E> changeStatusById(
            String userId, String id, Status status
    ) throws Exception;

    Optional<E> changeStatusByIndex(
            String userId, Integer index, Status status
    ) throws Exception;

    Optional<E> changeStatusByName(
            String userId, String name, Status status
    ) throws Exception;

    Optional<E> finishById(String userId, String id) throws Exception;

    Optional<E> finishByIndex(String userId, Integer index) throws Exception;

    Optional<E> finishByName(String userId, String name) throws Exception;

    Optional<E> startById(String userId, String id) throws Exception;

    Optional<E> startByIndex(String userId, Integer index) throws Exception;

    Optional<E> startByName(String userId, String name) throws Exception;

    Optional<E> updateById(
            String userId, String id, String name, String description
    ) throws Exception;

    Optional<E> updateByIndex(
            String userId, Integer index, String name, String description
    ) throws Exception;

}
