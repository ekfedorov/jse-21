package ru.ekfedorov.tm.exception;

public class AbstractException extends Exception {

    public AbstractException(String message) throws Exception {
        throw new Exception(message);
    }

}
