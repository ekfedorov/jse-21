package ru.ekfedorov.tm.exception.incorrect;

import ru.ekfedorov.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(Integer index) throws Exception {
        super("Error! " + (index + 1) + " less then 0 OR is empty...");
    }

}
